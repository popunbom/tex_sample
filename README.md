# tex-sample

This is LaTeX Report sample

## Requirements

- XeTeX
- bxjscls
- Fonts
   + Noto Sans
   + Noto Serif
   + Noto Sans CJK JP
   + Noto Serif CJK JP
   + Ricty Diminished Discord


## Author

Fumiya ENDOU @ SajiLab
