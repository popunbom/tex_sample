// Implementation of CvGLCM (Gray-Level-Co-Occurence-Matrix)
void CvGLCM::calcGLCM(int ai, int aj, cv::Mat &roi, int degree, int r) {
  int pix_i, pix_j;
  double N = 0.0;
  cv::Point2d d;

  GLCM &m = roiGLCM[ai][aj];

  m.glcm = cv::Mat(256, 256, CV_32FC1);

  for (int j = 0; j < roi.cols; j++) {
    for (int i = 0; i < roi.rows; i++) {
      d.x = (double) r * cos(degree * M_PI / 180.0);
      d.y = (double) r * sin(degree * M_PI / 180.0);

      if (0 < i + d.x && i + d.x < roi.rows && 0 < j + d.y && j + d.y < roi.cols) {
        pix_i = (int) roi.at < unsigned
        char > (j, i);
        pix_j = (int) roi.at < unsigned
        char > (j + d.y, i + d.x);
        m.glcm.at<double>(pix_i, pix_j)++;
        N++;
      }

    }
  }

  for (int j = 0; j < 255; j++) {
    for (int i = 0; i < 255; i++) {
      m.glcm.at<double>(j, i) /= N;
    }
  }

}


void CvGLCM::calcFeature(GLCM_FEATURE feature) {
  GLCM *matp;

  //fprintf(stderr, "Calculate features ... ");

  switch (feature) {
    case GLCM_ENR:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];
          matp->features = 0.0;
          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              matp->features += pow(matp->glcm.at<double>(L_i, L_j), 2);
            }
          }
        }
      }
      break;
    case GLCM_CNT:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];

          matp->features = 0.0;
          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              matp->features += pow(i - j, 2) * matp->glcm.at<double>(L_i, L_j);
            }
          }


          /*
          for (int k = 0; k < 256; k++) {
             double d1 = 0.0;
             for (int L_i = 0; L_i < 256; L_i++) {
              for (int L_j = 0; L_j < 256; L_j++) {
               if (abs(L_i - L_j) != k) break;
               else d1 += matp->glcm.at<double>(L_i, L_j);
              }
             }
             matp->features += k * k * d1;
          }
           */
        }
      }

      break;
    case GLCM_CRR:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          double d[5] = {0.0}, P_x, P_y;
          matp = &roiGLCM[j][i];

          // d[]
          P_x = 0.0;
          P_y = 0.0;
          for (int L_j = 0; L_j < 256; L_j++) {
            for (int L_i = 0; L_i < 256; L_i++) {
              d[0] += L_i * L_j * matp->glcm.at<double>(L_i, L_j);
              P_x += matp->glcm.at<double>(L_j, L_i);
              P_y += matp->glcm.at<double>(L_i, L_j);
            }
            d[1] += L_j * P_x;
            d[2] += L_j * P_y;
            d[3] += (L_j - d[1]) * (L_j - d[1]) * P_x;
            d[4] += (L_j - d[2]) * (L_j - d[2]) * P_y;
          }
          d[3] = sqrt(d[3]);
          d[4] = sqrt(d[4]);

          matp->features = (d[0] - d[1] * d[2]) / (d[3] * d[4]);
        }
      }
      break;
    case GLCM_VAR:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];

          double d1 = 0.0, d2 = 0.0;
          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              d2 += matp->glcm.at<double>(L_i, L_j);
            }
            d1 += L_i * d2;
          }

          matp->features = 0.0;
          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              matp->features += (L_i - d1) * (L_i - d1) * matp->glcm.at<double>(L_i, L_j);
            }
          }
        }
      }
      break;
    case GLCM_EPY:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];

          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              matp->features = -matp->glcm.at<double>(L_i, L_j) * log(matp->glcm.at<double>(L_i, L_j));
              fprintf(stderr, ":: %lf\n", -matp->glcm.at<double>(L_i, L_j));
              fprintf(stderr, "-- %lf\n", log(matp->glcm.at<double>(L_i, L_j)));
            }
          }
        }
      }
      break;
    case GLCM_SEP:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];

          double d1 = 0.0;
          matp->features = 0.0;
          for (int k = 0; k < (2 * 256 - 1); k++) {
            for (int L_i = 0; L_i < 256; L_i++) {
              for (int L_j = 0; L_j < 256; L_j++) {
                if (L_i + L_j != k) break;
                else {
                  d1 += matp->glcm.at<double>(L_i, L_j);
                }
              }
            }
            matp->features += -d1 * log(d1);
          }
        }
      }
      break;
    case GLCM_IDM:
      for (int j = 0; j < size_glcm.height; j++) {
        for (int i = 0; i < size_glcm.width; i++) {
          matp = &roiGLCM[j][i];

          matp->features = 0.0;
          for (int L_i = 0; L_i < 256; L_i++) {
            for (int L_j = 0; L_j < 256; L_j++) {
              matp->features += matp->glcm.at<double>(L_i, L_j) / (1.0 + pow(L_i - L_j, 2));
            }
          }
        }
      }
      break;
    default:
      break;
  }

//   fprintf(stderr, "done !\n");
}
