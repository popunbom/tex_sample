// ソースコードをファイルから入力する場合は、
// lstinputlisting 環境を使用します。
// インデントはスペース2文字分を推奨します。
// 1行32文字までで折り返しをしてください。

#include "stdio.h"

int fib(int n){
  if (n <= 1) return n;
  else return fib(n-1) + fib(n-2);
}

int main(int argc, char const *argv[])
{
  int i;

  for (i=0; i<10; i++)
    printf("fib(%d) = %d \n", i, fib(i));
  return 0;
}

